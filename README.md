### log
A small library for logging in C for UNIX-like systems

### Usage
#### Methods
```c
/* Set global level. TRACE, DEBUG, INFO, WARN, and ERROR */
log_set_global_level(int level)

/* Set whether should print date or not */
log_show_date(int state)

/* Set whether should add ANSI color or not in the output buffer */
log_show_color(int state)

/* Set whether should show the file name or not */
log_show_file(int state)

/* Set whether should show the line number or not */
log_show_line(int state)

/* Set whether should create a output file to write buffer content or not.
   Note that, you can specify NULL if you don't want to write buffer contents
*/
log_write_file(char *out)

/* Print output by specifying the level, file name, line number, and the message */
log_output(
    int level, const char *file,
    int line, const char *fmt, ...
)

/* These are macros which expands log_output(...) function */
log_trace(), log_debug(), log_info(), log_warn(), log_error()
```

#### Example
```c
#include <unistd.h>

#include "log.h"

int main()
{
    /* Settings */
    log_show_line(TRUE);
    log_show_color(TRUE);
    log_show_date(FALSE);
    log_show_file(TRUE);

    /* Macro calling */
    log_debug("Debugging...\n");
    sleep(3);
    log_warn("Oops, something went wrong, ignoring...\n");
    sleep(3);
    log_error("Encountered with an error. Error code: %d\n", 69);
    sleep(3);

    /* If you manually want to call log_output(...) */
    log_output(TRACE, __FILE__, __LINE__, "We traced!\n");
}
``` 