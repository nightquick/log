#ifndef LOG_H
#define LOG_H

#define BUFF_SZ         1024
#define INT_DIGITS      19

#ifndef TRUE
    #define TRUE    1
#endif
#ifndef FALSE
    #define FALSE   0
#endif

#define TRACE   0
#define DEBUG   1
#define INFO    2
#define WARN    3
#define ERROR   4
#define IWHITE  5
#define DWHITE  6
#define ENDCLR  7

/*
    Since va_start(...) requires __VA_ARGS__,
    we've to call vprintf(...) again just for the formatter
*/
#define logy_raw_print(fmt) \
    va_list arg;            \
    va_start(arg, fmt);     \
    vprintf(fmt, arg);      \
    va_end(arg);

#define logy_raw_write(out, fmt) \
    va_list arg;                 \
    va_start(arg, fmt);          \
    vfprintf(out, fmt, arg);     \
    va_end(arg);

#define log_trace(...) log_output(TRACE, __FILE__, __LINE__, __VA_ARGS__)
#define log_debug(...) log_output(DEBUG, __FILE__, __LINE__, __VA_ARGS__)
#define log_info(...)  log_output(INFO,  __FILE__, __LINE__, __VA_ARGS__)
#define log_warn(...)  log_output(WARN, __FILE__, __LINE__, __VA_ARGS__)
#define log_error(...) log_output(ERROR, __FILE__, __LINE__, __VA_ARGS__)

void log_show_line(int state);
void log_show_color(int state);
void log_show_date(int state);
void log_show_file(int state);
void log_write_file(char *out);
void log_set_global_level(int level);

void log_output(
    int level, const char *file,
    int line, const char *fmt, ...
);
#endif /* LOG_H */
