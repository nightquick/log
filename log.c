#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <time.h>

#include "log.h"

struct Logger {
    int level;
    int show_date;
    int show_color;
    int show_file;
    int show_line;
    char *file_path;
};

struct Logger logger = {0};

/*
    Setup both arrays with colors and level names
    level_colors and level_labels
*/
const char *level_colors[] = {
    "\033[0;92m", "\033[0;93m", "\033[0;96m",
    "\033[0;31m", "\033[0;91m", "\033[0;97m",
    "\033[0;37m", "\033[0m"
};

const char *level_label[] = {
    "TRACE", "DEBUG", "INFO", "WARN", "ERROR"
};

void log_set_global_level(int level)
{
    logger.level = level;
}

void log_show_date(int state)
{
    logger.show_date = state;
}

void log_show_color(int state)
{
    logger.show_color = state;
}

void log_show_file(int state)
{
    logger.show_file = state;
}

void log_show_line(int state)
{
    logger.show_line = state;
}

void log_write_file(char *out)
{
    logger.file_path = out;
}

void logy_print(const char *fmt, ...)
{
    va_list arg;

    va_start(arg, fmt);
    vprintf(fmt, arg);
    va_end(arg);
}

void logy_append(char *dst, const char *fmt, ...)
{
    va_list arg;
    char tmp[BUFF_SZ];

    memset(tmp, '\0', sizeof(tmp));

    va_start(arg, fmt);
    vsprintf(tmp, fmt, arg);
    strcat(dst, tmp);
    va_end(arg);
}

char *logy_fmt_date(void)
{
    struct tm *tm;
    time_t ti;
    static char buf[12];

    ti = time(NULL);
    tm = localtime(&ti);

    memset(buf, '\0', sizeof(buf));
    logy_append(buf,
        "%d:%02d:%02d ", tm->tm_year + 1900, tm->tm_mon + 1,
        tm->tm_wday + 7
    );

    return buf;
}

char *logy_itox(int i)
{
    static char buf[INT_DIGITS + 2];
    char *p;
    int t;

    p = buf + 19 + 1;
    t = i;

    memset(buf, '\0', sizeof(buf));

    do {
        *--p = i >= 0 ? '0' + (i % 10) : '0' - (i % 10);
        i /= 10;
    } while (i != 0);

    t < 0 ? *--p = '-' : ' '; /* For negative values */
    return p;
}

void log_output(
    int level, const char *file,
    int line, const char *fmt, ...
)
{
    struct tm *tm;
    time_t ti;
    FILE *fp;
    char buf[BUFF_SZ];

    ti = time(NULL);
    tm = localtime(&ti);

    logger.level = level;

    memset(buf, '\0', sizeof(buf));

    logy_append(buf, "%s%s%02d:%02d:%02d%s %s %s%s%s%s ",
        logger.show_color ? level_colors[DWHITE] : "",
        logger.show_date ? logy_fmt_date() : "",
        tm->tm_hour, tm->tm_min, tm->tm_sec,
        logger.show_color ? level_colors[logger.level] : "",
        level_label[logger.level],
        logger.show_color ? level_colors[DWHITE] : "",
        logger.show_file ? file : "",
        logger.show_line ? ":" : "",
        logger.show_line ? logy_itox(line) : ""
    );

    if (logger.file_path) {
        fp = fopen(logger.file_path, "a+");

        if (fp == NULL) {
            perror("fopen()");
            return;
        }

        fprintf(fp,
            "%s%02d:%02d:%02d %s %s%s%s ",
            logger.show_date ? logy_fmt_date() : "",
            tm->tm_hour, tm->tm_min, tm->tm_sec,
            level_label[logger.level],
            logger.show_file ? file : "",
            logger.show_line ? ":" : "",
            logger.show_line ? logy_itox(line) : ""
        );
        logy_raw_write(fp, fmt);
        fclose(fp);
    }

    logy_print("%s%s", buf, level_colors[IWHITE]);
    logy_raw_print(fmt);
    logy_print("%s", level_colors[ENDCLR]);
}
